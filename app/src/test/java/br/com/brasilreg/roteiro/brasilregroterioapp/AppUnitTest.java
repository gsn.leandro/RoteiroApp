package br.com.brasilreg.roteiro.brasilregroterioapp;

import org.junit.Test;

import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.util.DateUtil;
import br.com.brasilreg.roteiro.app.util.HourUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AppUnitTest {

    @Test
    public void test_onlyNumbers_1() {
        String phone = "(41) 9 9662-8920";
        phone = phone.replace("-", "");
        phone = phone.replace("(", "");
        phone = phone.replace(")", "");
        phone = phone.replace(" ", "");
        phone = phone.replace(".", "");

        assertTrue(phone.matches("[0-9]+"));
    }

    @Test
    public void test_onlyNumbers_2() {
        String phone = "(41) 9 9662-8920";
        phone = phone.replaceAll("( )-.+", "").trim();

        assertTrue(phone.matches("[0-9]+"));
    }

    @Test
    public void phone_onlyNumbers() {
        VehicleInspection inspection = new VehicleInspection();
        inspection.setPhone("(41) 9 9662-8920");

        assertTrue(inspection.phoneIsValid());
    }

    @Test
    public void formatLimitDate() {
        String dateToFormat = "2021-08-27T00:00:00.000-03:00";

        assertEquals("27/08/2021", DateUtil.formatLimitDate(dateToFormat));
    }

    @Test
    public void formatAppointment() {
        String appointment = "2021-08-30T18:29:48.000-03:00";

        assertEquals("18:29", HourUtil.formatAppointment( appointment ) ) ;
    }
}