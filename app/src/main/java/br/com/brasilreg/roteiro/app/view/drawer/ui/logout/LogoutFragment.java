package br.com.brasilreg.roteiro.app.view.drawer.ui.logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.Preferences;
import br.com.brasilreg.roteiro.app.dao.SurveyorDAO;
import br.com.brasilreg.roteiro.app.dao.VehicleInspectionDAO;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;
import br.com.brasilreg.roteiro.app.domain.Surveyor;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.util.ConstantUtil;

public class LogoutFragment extends Fragment {

    private View root;
    private Button logout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_logout, container, false);

        initUI();
        onClickListener();

        return root;
    }

    private void onClickListener() {
        logout.setOnClickListener( v -> {
            SurveyorDAO.deleteAll();
            VehicleInspectionDAO.deleteAll();

            Preferences.deleteUserPreferences( this );

            startActivity( new Intent( getActivity(), LoginActivity.class ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
        });
    }

    private void initUI() {
        logout = root.findViewById( R.id.button_logout );
    }
}