package br.com.brasilreg.roteiro.app.util;

@SuppressWarnings("SpellCheckingInspection")
public class ConstantUtil {

    public static final String PREFERENCES_AUTO_LOGIN = "PREFERENCES_AUTO_LOGIN";
    public static final String PREFERENCES_NAME_LOGIN = "login";
    public static final String URL_BRASILREG_SERVER = "ENDPOINT";
}
