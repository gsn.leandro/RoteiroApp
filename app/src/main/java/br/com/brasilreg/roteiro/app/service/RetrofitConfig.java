package br.com.brasilreg.roteiro.app.service;

import com.google.gson.GsonBuilder;

import br.com.brasilreg.roteiro.app.util.ConstantUtil;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {

    private final Retrofit retrofit;

    public RetrofitConfig() {
        retrofit = new Retrofit.Builder()
                .baseUrl( ConstantUtil.URL_BRASILREG_SERVER )
                .addConverterFactory( GsonConverterFactory.create( new GsonBuilder().create() ) )
                .build();
    }

    public BrasilRegService getConfig() {
        return this.retrofit.create( BrasilRegService.class );
    }

}
