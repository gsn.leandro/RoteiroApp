package br.com.brasilreg.roteiro.app.view.nht;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.SurveyorDAO;
import br.com.brasilreg.roteiro.app.dao.VehicleInspectionDAO;
import br.com.brasilreg.roteiro.app.domain.Surveyor;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.service.RequestBody;
import br.com.brasilreg.roteiro.app.service.RetrofitConfig;
import br.com.brasilreg.roteiro.app.service.response.BaseResponse;
import br.com.brasilreg.roteiro.app.view.drawer.DrawerActivity;
import br.com.brasilreg.roteiro.app.view.inspection.InspectionViewActivity;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static br.com.brasilreg.roteiro.app.R.color.background_nht_rejected;

public class NHTRequestActivity extends AppCompatActivity {

    Button cancel;
    Button request;

    EditText reason;

    TextView inspectionID;
    TextView warningMessage;
    TextView countCharacters;

    Surveyor surveyor;
    ProgressBar progressBar;
    VehicleInspection inspection;

    TextWatcher afterTextChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nht_request);
        surveyor    = SurveyorDAO.getSurveyor();
        inspection  = VehicleInspectionDAO.getByID( getIntent().getLongExtra("id", 1L) );

        initUI();
        updateUI();
        textWatcher();
        clickListener();
    }

    private void clickListener() {
        cancel.setOnClickListener(v -> {
            startActivity( new Intent( NHTRequestActivity.this, InspectionViewActivity.class ).putExtra( "id", inspection.getId() ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
        });

        request.setOnClickListener(v -> {
            request.setEnabled( false );
            progressBar.setVisibility( View.VISIBLE );

            Call<BaseResponse> call = new RetrofitConfig().getConfig().requestNHT( RequestBody.doRequestNHT( surveyor.getTOKEN(), inspection.getiID(), reason.getText().toString() ) );
            call.enqueue( new NHTRequestCallBack( NHTRequestActivity.this ) );
        });
    }

    private void textWatcher() {
        afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { } // ignore

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { } // ignore

            @Override
            public void afterTextChanged( Editable editable ) {
                int count = reason.getText().toString().length();

                if ( count >= 20 ) {
                    request.setEnabled( true );
                    warningMessage.setVisibility( View.INVISIBLE );
                    request.setBackgroundTintList( ContextCompat.getColorStateList(getBaseContext(), R.color.white) );
                } else {
                    request.setEnabled( false );
                    warningMessage.setVisibility( View.VISIBLE );
                    request.setBackgroundTintList( ContextCompat.getColorStateList(getBaseContext(), background_nht_rejected ) );
                }

                countCharacters.setText( String.format( "%s %s", getResources().getString(R.string.nht_request_characters_count), count ) );
            }
        };

        reason.addTextChangedListener( afterTextChangedListener );
    }

    private void updateUI() {
        request.setEnabled( false );
        request.setBackgroundTintList( ContextCompat.getColorStateList(getBaseContext(), background_nht_rejected ) );
        countCharacters.setText( String.format( "%s 0", getResources().getString(R.string.nht_request_characters_count) ) );
        inspectionID.setText( String.format( "%s %s", getResources().getString(R.string.card_inspection_id), inspection.getInspectionID() ) );
    }

    private void initUI() {
        cancel          = findViewById( R.id.bt_nht_request_cancel);
        reason          = findViewById( R.id.et_reason);
        request         = findViewById( R.id.bt_nht_request );
        progressBar     = findViewById( R.id.pb_nht_request );
        inspectionID    = findViewById( R.id.tv_nht_request_id );
        warningMessage  = findViewById( R.id.tv_nht_request_warning );
        countCharacters = findViewById( R.id.tv_nht_request_count);
    }

    private static class NHTRequestCallBack implements Callback<BaseResponse> {

        private final WeakReference<NHTRequestActivity> reference;

        NHTRequestCallBack( NHTRequestActivity context ) {
            reference = new WeakReference<>(context);
        }

        @Override
        public void onResponse(@NotNull Call<BaseResponse> call, @NotNull Response<BaseResponse> response ) {
            /* Codigos: 1 Sucesso, -1 Credenciais inválidas, 2 Não permitido */
            reference.get().request.setEnabled( true );
            reference.get().progressBar.setVisibility( View.GONE );
            if ( response.body() == null )
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
            else if ( response.body().getCode() == 1 ) {
                reference.get().inspection.setNHT( 1 );
                reference.get().inspection.save();
                reference.get().startActivity( new Intent( reference.get(), DrawerActivity.class ).putExtra( "message", reference.get().getResources().getString( R.string.msg_nht_success ) ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            } else if ( response.body().getCode() == -1 )
                reference.get().startActivity( new Intent( reference.get(), LoginActivity.class ).putExtra( "message", reference.get().getResources().getString( R.string.msg_token_expired ) ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            else
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_invalid_credentials ), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(@NotNull Call<BaseResponse> call, @NotNull Throwable t) {
            reference.get().request.setEnabled( true );
            reference.get().progressBar.setVisibility( View.GONE );
            Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
        }
    }
}