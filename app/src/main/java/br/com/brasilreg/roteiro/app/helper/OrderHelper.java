package br.com.brasilreg.roteiro.app.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.brasilreg.roteiro.app.domain.VehicleInspection;

public class OrderHelper {

    public static List<VehicleInspection> orderByPriority(List<VehicleInspection> inspections ) {
        List<VehicleInspection> listOrdered = new ArrayList<>();

        // Separando em Status primeiro
        for ( int status = 0; status < 4; status++ ) {
            List<VehicleInspection> auxList = new ArrayList<>();
            for ( VehicleInspection inspection : inspections ) {
                if ( inspection.getStatus() == status )
                    auxList.add( inspection );
            }

            Collections.sort(auxList);

            listOrdered.addAll(auxList);
        }

        return listOrdered;
    }

}
