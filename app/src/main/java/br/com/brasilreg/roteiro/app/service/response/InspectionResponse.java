package br.com.brasilreg.roteiro.app.service.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import br.com.brasilreg.roteiro.app.domain.VehicleInspection;

public class InspectionResponse extends BaseResponse {

    @SerializedName("body")
    @Expose
    private List<VehicleInspection> inspection;

    public InspectionResponse() { }

    public InspectionResponse( List<VehicleInspection> inspection ) {
        this.inspection = inspection;
    }

    public List<VehicleInspection> getInspection() {
        return inspection;
    }

    public void setInspection(List<VehicleInspection> inspection) {
        this.inspection = inspection;
    }

    @Override
    public String toString() {
        return "InspectionResponse{" +
                "code=" + getCode() +
                ", message='" + getMessage() + '\'' +
                ", inspection=" + inspection +
                '}';
    }
}
