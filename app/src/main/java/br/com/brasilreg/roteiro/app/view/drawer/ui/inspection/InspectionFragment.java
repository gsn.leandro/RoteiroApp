package br.com.brasilreg.roteiro.app.view.drawer.ui.inspection;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.SurveyorDAO;
import br.com.brasilreg.roteiro.app.dao.VehicleInspectionDAO;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.helper.OrderHelper;
import br.com.brasilreg.roteiro.app.service.RequestBody;
import br.com.brasilreg.roteiro.app.service.RetrofitConfig;
import br.com.brasilreg.roteiro.app.service.response.InspectionResponse;
import br.com.brasilreg.roteiro.app.util.DateUtil;
import br.com.brasilreg.roteiro.app.util.HourUtil;
import br.com.brasilreg.roteiro.app.view.inspection.InspectionAdapter;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InspectionFragment extends Fragment {

    View root;
    Button update;
    ProgressBar progressBar;
    RecyclerView recycler;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_inspection, container, false);

        initUI();
        clickListener();
        getInspections();

        createRecycler();

        return root;
    }

    private void createRecycler() {
        recycler.setLayoutManager( new LinearLayoutManager( getContext() ) );
        recycler.setHasFixedSize(true);
        recycler.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled( recyclerView, dx, dy );
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        } );

        reloadUI();
    }

    private void getInspections() {
        if ( VehicleInspectionDAO.listAll().size() == 0 )
            getInspectionsFromServer( getView() );
    }

    private void getInspectionsFromServer( View view ) {
        progressBar.setVisibility( View.VISIBLE );
        update.setEnabled( false );

        Call<InspectionResponse> call = new RetrofitConfig().getConfig().listInspections( RequestBody.doGETInspections( SurveyorDAO.getSurveyor().getTOKEN() ) );
        call.enqueue( new ListInspectionsCallBack( InspectionFragment.this, view ) );
    }

    private void clickListener() {
        update.setOnClickListener(this::getInspectionsFromServer);
    }

    private void reloadUI() {
        recycler.setAdapter( new InspectionAdapter( getContext(), OrderHelper.orderByPriority( VehicleInspectionDAO.listAll() ) ) );
    }

    private void initUI() {
        update      = root.findViewById( R.id.button_update_inspection );
        recycler    = root.findViewById( R.id.recycler_inspection );
        progressBar = root.findViewById( R.id.pb_inspection );
    }

    private static class ListInspectionsCallBack implements Callback<InspectionResponse> {

        private final WeakReference<InspectionFragment> reference;
        private final View view;

        ListInspectionsCallBack( InspectionFragment context, View v ) {
            reference = new WeakReference<>(context);
            view = v;
        }

        @Override
        public void onResponse(@NotNull Call<InspectionResponse> call, Response<InspectionResponse> response) {
            /* Codigos: 1 Sucesso, -1 Credenciais inválidas */
            reference.get().progressBar.setVisibility(View.GONE);
            reference.get().update.setEnabled( true );
            if ( response.body() == null )
                Snackbar.make( view, reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
            else if ( response.body().getCode() == 1 ) {
                VehicleInspectionDAO.deleteAll();

                for ( VehicleInspection inspection : response.body().getInspection() ) {
                    inspection.setLimitDate( DateUtil.formatLimitDate( inspection.getLimitDate() ) );
                    inspection.setAppointment( HourUtil.formatAppointment( inspection.getAppointment() ) );
                    VehicleInspectionDAO.save( inspection );
                }

                reference.get().reloadUI();
            } else if ( response.body().getCode() == -1 )
                reference.get().startActivity( new Intent( reference.get().getActivity(), LoginActivity.class ).putExtra( "message", reference.get().getResources().getString( R.string.msg_token_expired ) ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            else
                Snackbar.make( view, reference.get().getResources().getString( R.string.msg_invalid_credentials ), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(@NotNull Call<InspectionResponse> call, @NotNull Throwable t) {
            reference.get().update.setEnabled( true );
            reference.get().progressBar.setVisibility(View.GONE);
            Snackbar.make( view, reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
        }
    }
}