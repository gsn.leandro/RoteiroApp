package br.com.brasilreg.roteiro.app.service;

import java.util.HashMap;
import java.util.Map;

public class RequestBody {

    public static Map<String, String> doLogin( String user, String password ) {
        Map<String,String> hashMap = new HashMap<>();

        hashMap.put( "login", user );
        hashMap.put( "senha", password );

        return hashMap;
    }

    public static Map<String, String> doUpdatePassword( String user, String oldPassword, String newPassword ) {
        Map<String,String> hashMap = new HashMap<>();

        hashMap.put( "login", user );
        hashMap.put( "senha", oldPassword );
        hashMap.put( "novaSenha", newPassword );

        return hashMap;
    }

    public static Map<String, String> doForgotPassword( String user ) {
        Map<String,String> hashMap = new HashMap<>();

        hashMap.put( "login", user );

        return hashMap;
    }

    public static Map<String, String> doRequestNHT( String token, int id, String reason ) {
        Map<String,String> hashMap = new HashMap<>();

        hashMap.put( "token", token );
        hashMap.put( "id", String.valueOf( id ) );
        hashMap.put( "motivo", reason );

        return hashMap;
    }

    public static Map<String, String> doGETInspections( String token ) {
        Map<String,String> hashMap = new HashMap<>();

        hashMap.put( "token", token );

        return hashMap;
    }
}
