package br.com.brasilreg.roteiro.app.util;

import br.com.brasilreg.roteiro.app.R;

public class StatusUtil {

    public static int getStatusColor( int status ) {
        switch ( status ) {
            case 0:
                return R.color.status_red;
            case 1:
                return R.color.status_yellow;
            case 2:
                return R.color.status_blue;
            case 3:
            default:
                return R.color.status_green;
        }
    }
}
