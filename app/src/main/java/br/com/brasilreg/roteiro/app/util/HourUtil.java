package br.com.brasilreg.roteiro.app.util;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class HourUtil {

    public static String formatHour( String appointment ) {
        String[] aux = appointment.split(":");

        return ( ( aux[0].length() == 1) ? ("0" + aux[0]) : aux[0] ) + ":" + ( ( aux[1].length() == 1) ? ("0" + aux[1]) : aux[1] );
    }

    public static String formatAppointment(String appointment) {
        if ( appointment == null )
            return "24:00";

        DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
        LocalDateTime parsedDate = dtf.parseLocalDateTime(appointment);

        return HourUtil.formatHour( parsedDate.toString( DateTimeFormat.forPattern( "HH:mm" ) ) );
    }

}
