package br.com.brasilreg.roteiro.app.service.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("codigo")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;

    public BaseResponse() { }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
