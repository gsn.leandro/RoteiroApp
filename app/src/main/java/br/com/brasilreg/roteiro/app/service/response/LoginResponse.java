package br.com.brasilreg.roteiro.app.service.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.brasilreg.roteiro.app.domain.Surveyor;

@SuppressWarnings({"SpellCheckingInspection"})
public class LoginResponse extends BaseResponse {

    @SerializedName("body")
    @Expose
    private Surveyor surveyor;

    public LoginResponse() { }

    public Surveyor getSurveyor() {
        return surveyor;
    }

    public void setSurveyor(Surveyor body) {
        this.surveyor = body;
    }

    @Override
    public String toString() {
        return "ResponseService{" +
                "codigo=" + getCode() +
                ", Body='" + surveyor + '\'' +
                ", message='" + getMessage() + '\'' +
                '}';
    }
}
