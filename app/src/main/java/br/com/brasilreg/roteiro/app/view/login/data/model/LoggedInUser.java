package br.com.brasilreg.roteiro.app.view.login.data.model;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    private final String userId;
    private final String userMail;
    private final String displayName;

    public LoggedInUser( String userId, String displayName, String userMail ) {
        this.userId = userId;
        this.userMail = userMail;
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUserMail() {
        return userMail;
    }
}