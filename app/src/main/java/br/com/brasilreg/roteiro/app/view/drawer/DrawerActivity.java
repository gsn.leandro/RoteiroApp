package br.com.brasilreg.roteiro.app.view.drawer;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import br.com.brasilreg.roteiro.app.BuildConfig;
import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.SurveyorDAO;
import br.com.brasilreg.roteiro.app.domain.Surveyor;

public class DrawerActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private Surveyor surveyor;

    private TextView surveyorName;
    private TextView surveyorEmail;
    private TextView appVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        surveyor = SurveyorDAO.getSurveyor();
        String message = getIntent().getStringExtra("message");
        if ( message != null )
            Snackbar.make( findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT ).show();
        
        initToolBar();
        initDrawer();
        inflateUI();
    }

    private void initToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void inflateUI() {
        appVersion.setText( String.format( "%s %s", getResources().getString( R.string.app_version ), BuildConfig.VERSION_NAME) );
        surveyorName.setText( surveyor.getName() );
        surveyorEmail.setText( surveyor.getEmail() );
    }

    private void initDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_inspection, R.id.nav_gallery, R.id.nav_logout)
                .setOpenableLayout(drawer)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        initUI( navigationView.getHeaderView(0) );
    }

    private void initUI( View headerView ) {
        appVersion      = headerView.findViewById( R.id.textVersion );
        surveyorName    = headerView.findViewById( R.id.nav_header_name );
        surveyorEmail   = headerView.findViewById( R.id.textEmail );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        System.exit(0);
    }
}