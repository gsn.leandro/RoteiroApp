package br.com.brasilreg.roteiro.app.view.password;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.service.RequestBody;
import br.com.brasilreg.roteiro.app.service.RetrofitConfig;
import br.com.brasilreg.roteiro.app.service.response.LoginResponse;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    private Button cancel;
    private Button forgot;
    private EditText user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initUI();
        clickListener();
    }

    private void clickListener() {

        forgot.setOnClickListener(v -> {
            forgot.setEnabled( false );
            Call<LoginResponse> call = new RetrofitConfig().getConfig().forgotPassword( RequestBody.doForgotPassword( user.getText().toString() ) );
            call.enqueue( new ForgotPasswordCallBack( ForgotPasswordActivity.this ) );
        });

        cancel.setOnClickListener(v -> startActivity( new Intent( ForgotPasswordActivity.this, LoginActivity.class ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) ));
    }

    private void initUI() {
        user    = findViewById( R.id.et_forgot_password);
        cancel  = findViewById( R.id.bt_nht_request_cancel);
        forgot  = findViewById( R.id.bt_forgot_password);
    }

    private static class ForgotPasswordCallBack implements Callback<LoginResponse> {

        private final WeakReference<ForgotPasswordActivity> reference;

        ForgotPasswordCallBack( ForgotPasswordActivity context ) {
            reference = new WeakReference<>(context);
        }

        @Override
        public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response ) {
            /* Codigos: 1 Sucesso, -1 Credenciais inválidas */

            reference.get().forgot.setEnabled( true );
            if ( response.body() == null )
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
            else if ( response.body().getCode() == 1 )
                reference.get().startActivity( new Intent( reference.get(), LoginActivity.class ).putExtra( "message", reference.get().getResources().getString( R.string.msg_forgot_password_success ) ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            else
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_invalid_credentials ), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
            reference.get().forgot.setEnabled( true );
            Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
        }
    }
}