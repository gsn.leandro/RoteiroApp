package br.com.brasilreg.roteiro.app.view.splash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Bundle;

import com.facebook.stetho.Stetho;

import br.com.brasilreg.roteiro.app.BuildConfig;
import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        noModeNightUser();

        if ( BuildConfig.DEBUG )
            Stetho.initializeWithDefaults( this );

        Thread timerThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3500);
                } catch (Exception error) {
                    error.printStackTrace();
                } finally {
                    startActivity( new Intent( SplashScreenActivity.this, LoginActivity.class ) );
                }
            }
        };

        timerThread.start();
    }

    private void noModeNightUser() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}