package br.com.brasilreg.roteiro.app.dao;

import java.util.List;

import br.com.brasilreg.roteiro.app.domain.Surveyor;

public class SurveyorDAO {

    public static void deleteAll() {
        Surveyor.deleteAll( Surveyor.class );
    }

    public static void save( Surveyor surveyor ) {
        surveyor.save();
    }

    public static Surveyor getSurveyor() {
        List<Surveyor> list = Surveyor.listAll( Surveyor.class );

        return list.get(0);
    }
}
