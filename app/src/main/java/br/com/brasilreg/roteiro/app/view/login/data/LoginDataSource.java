package br.com.brasilreg.roteiro.app.view.login.data;

import java.io.IOException;

import br.com.brasilreg.roteiro.app.view.login.data.model.LoggedInUser;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    LoggedInUser logged;

    public Result<LoggedInUser> login( String username, String password ) {

        try {
            // TODO: handle loggedInUser authentication
            logged = new LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe", "Jane.Doe@brasilreg.com.br");
            return new Result.Success<>(logged);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }

}