package br.com.brasilreg.roteiro.app.view.drawer.ui.employee;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import br.com.brasilreg.roteiro.app.R;

public class EmployeeFragment extends Fragment {

    @SuppressWarnings("UnnecessaryLocalVariable")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_employee, container, false);
        //TODO Demanda futura
        return root;
    }
}