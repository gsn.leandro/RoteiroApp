package br.com.brasilreg.roteiro.app.view.password;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.service.RequestBody;
import br.com.brasilreg.roteiro.app.service.RetrofitConfig;
import br.com.brasilreg.roteiro.app.service.response.LoginResponse;
import br.com.brasilreg.roteiro.app.view.login.ui.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends AppCompatActivity {

    private Button update;
    private Button cancel;
    private TextView name;
    private EditText password;
    private ImageView showPassword;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        String user        = getIntent().getStringExtra("user");
        String passwordOld = getIntent().getStringExtra("password");

        initUI();
        inflateUI( user );
        clickListener( user, passwordOld);
    }

    private void inflateUI( String user ) {
        name.setText( String.format( "%s %s", getApplicationContext().getResources().getString(R.string.update_password_name), user ) );
    }

    private void clickListener( String user, String passwordOld ) {

        showPassword.setOnClickListener(v -> {
            if ( password.getTransformationMethod() != null )
                password.setTransformationMethod( null );
            else
                password.setTransformationMethod( new PasswordTransformationMethod() );

            password.setSelection( password.getText().length() );
        });

        update.setOnClickListener(v -> {
            update.setEnabled( false );
            Call<LoginResponse> call = new RetrofitConfig().getConfig().updatePassword( RequestBody.doUpdatePassword( user, passwordOld, password.getText().toString() ) );
            call.enqueue( new UpdatePasswordCallBack( UpdatePasswordActivity.this ) );
        });

        cancel.setOnClickListener(v -> startActivity( new Intent( UpdatePasswordActivity.this, LoginActivity.class ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) ));
    }

    private void initUI() {
        name            = findViewById( R.id.tv_update_password_name );
        update          = findViewById( R.id.bt_forgot_password);
        cancel          = findViewById( R.id.bt_nht_request_cancel);
        password        = findViewById( R.id.et_forgot_password);
        showPassword    = findViewById( R.id.iv_update_password );
    }

    private static class UpdatePasswordCallBack implements Callback<LoginResponse> {

        private final WeakReference<UpdatePasswordActivity> reference;

        UpdatePasswordCallBack( UpdatePasswordActivity context ) {
            reference = new WeakReference<>(context);
        }

        @Override
        public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response ) {
            /* Codigos: 1 Sucesso, -1 Credenciais inválidas */
            reference.get().update.setEnabled( true );
            if ( response.body() == null )
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
            else if ( response.body().getCode() == 1 ) {
                reference.get().startActivity( new Intent( reference.get(), LoginActivity.class ).putExtra( "message", reference.get().getResources().getString( R.string.msg_update_password_success ) ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            } else
                Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_invalid_credentials ), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
            reference.get().update.setEnabled( true );
            Snackbar.make( reference.get().getCurrentFocus(), reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
        }
    }

}