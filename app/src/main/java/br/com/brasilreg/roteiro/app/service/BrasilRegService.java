package br.com.brasilreg.roteiro.app.service;

import java.util.Map;

import br.com.brasilreg.roteiro.app.service.response.BaseResponse;
import br.com.brasilreg.roteiro.app.service.response.InspectionResponse;
import br.com.brasilreg.roteiro.app.service.response.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface BrasilRegService {

    @POST("login")
    Call<LoginResponse> login( @Body Map<String, String> param );

    @POST("register")
    Call<LoginResponse> updatePassword( @Body Map<String, String> param );

    @POST("forgot")
    Call<LoginResponse> forgotPassword( @Body Map<String, String> param );

    @POST("update")
    Call<InspectionResponse> listInspections(@Body Map<String, String> param );

    @POST("nht")
    Call<BaseResponse> requestNHT(@Body Map<String, String> param );
}
