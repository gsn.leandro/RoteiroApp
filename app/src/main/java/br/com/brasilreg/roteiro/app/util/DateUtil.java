package br.com.brasilreg.roteiro.app.util;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class DateUtil {

    public static String formatLimitDate( String limitDate ) {
        DateTimeFormatter dtf = ISODateTimeFormat.dateTime();
        LocalDateTime parsedDate = dtf.parseLocalDateTime(limitDate);

        return parsedDate.toString( DateTimeFormat.forPattern( "dd/MM/yyyy" ) );
    }
}
