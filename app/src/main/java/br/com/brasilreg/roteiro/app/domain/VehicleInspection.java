package br.com.brasilreg.roteiro.app.domain;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.brasilreg.roteiro.app.util.NumberUtil;

public class VehicleInspection extends SugarRecord<Surveyor> implements Comparable<VehicleInspection> {

    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("vistoria_id")
    @Expose
    int iID;
    @SerializedName("nht")
    @Expose
    int NHT;
    @SerializedName("nhtCount")
    @Expose
    int countNHT;
    @SerializedName("estado")
    @Expose
    String UF;
    @SerializedName("cidade")
    @Expose
    String city;
    @SerializedName("tipo")
    @Expose
    String type;
    @SerializedName("telefone")
    @Expose
    String phone;
    @SerializedName("sistema")
    @Expose
    String system;
    @SerializedName("versao")
    @Expose
    String version;
    @SerializedName("placa")
    @Expose
    String carPlate;
    @SerializedName("seguradora")
    @Expose
    String companyName;
    @SerializedName("oficina")
    @Expose
    String repairShopName;
    @SerializedName("sinistro")
    @Expose
    String inspectionID; //Sinistro
    @SerializedName("endereco")
    @Expose
    String repairShopAddress;
    @SerializedName("observacao")
    @Expose
    String observation;
    @SerializedName("data")
    @Expose
    String limitDate;
    @SerializedName("agendamento")
    @Expose
    String appointment;

    public VehicleInspection() { }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public int getiID() {
        return iID;
    }

    public void setiID(int iID) {
        this.iID = iID;
    }

    public int getNHT() {
        return NHT;
    }

    public void setNHT( int NHT ) {
        /*
        0 - Sem Pedido; 1- Pedido; 2 - Rejeitado
         */
        this.NHT = NHT;
    }

    public int getCountNHT() {
        return countNHT;
    }

    public void setCountNHT(int countNHT) {
        this.countNHT = countNHT;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVersion() {
        return version == null ? "-" : version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCarPlate() {
        return carPlate;
    }

    public void setCarPlate(String carPlate) {
        this.carPlate = carPlate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRepairShopName() {
        return repairShopName;
    }

    public void setRepairShopName(String repairShopName) {
        this.repairShopName = repairShopName;
    }

    public String getInspectionID() {
        return inspectionID;
    }

    public void setInspectionID(String inspectionID) {
        this.inspectionID = inspectionID;
    }

    public String getRepairShopAddress() {
        return repairShopAddress;
    }

    public void setRepairShopAddress(String repairShopAddress) {
        this.repairShopAddress = repairShopAddress;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    @Override
    public String toString() {
        return "VehicleInspection{" +
                "status=" + status +
                ", system=" + system +
                ", iID=" + iID +
                ", NHT=" + NHT +
                ", countNHT=" + countNHT +
                ", UF='" + UF + '\'' +
                ", city='" + city + '\'' +
                ", type='" + type + '\'' +
                ", phone='" + phone + '\'' +
                ", version='" + version + '\'' +
                ", carPlate='" + carPlate + '\'' +
                ", companyName='" + companyName + '\'' +
                ", repairShopName='" + repairShopName + '\'' +
                ", inspectionID='" + inspectionID + '\'' +
                ", repairShopAddress='" + repairShopAddress + '\'' +
                ", observation='" + observation + '\'' +
                ", limitDate='" + limitDate + '\'' +
                ", appointment='" + appointment + '\'' +
                '}';
    }

    @Override
    public int compareTo( VehicleInspection o ) {
        try {
            VehicleInspection p1 = this;

            int sComp = p1.getLimitDate().compareTo( o.getLimitDate() );

            if ( sComp != 0 )
                return sComp;

            if ( p1.getAppointment() == null )
                p1.setAppointment( "24:00" );
            if ( o.getAppointment() == null )
                o.setAppointment( "24:00" );

            return new SimpleDateFormat("hh:mm", Locale.ENGLISH).parse( p1.getAppointment() ).compareTo( new SimpleDateFormat("hh:mm", Locale.ENGLISH ).parse( o.getAppointment() ) );
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public boolean phoneIsValid() {
        String phone = NumberUtil.formatCallNumber( this.phone );

        return phone.matches("[0-9]+");
    }
}
