package br.com.brasilreg.roteiro.app.dao;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.fragment.app.Fragment;

import br.com.brasilreg.roteiro.app.util.ConstantUtil;

public class Preferences {

    public static void deleteUserPreferences( Fragment fragment ) {
        SharedPreferences sharedPref = fragment.requireActivity().getSharedPreferences( ConstantUtil.PREFERENCES_NAME_LOGIN, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean( ConstantUtil.PREFERENCES_AUTO_LOGIN, false );
        editor.apply();
    }

    public static void savePreferences( boolean checked, Context context ) {
        SharedPreferences sharedPref = context.getSharedPreferences( ConstantUtil.PREFERENCES_NAME_LOGIN, Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean( ConstantUtil.PREFERENCES_AUTO_LOGIN, checked );
        editor.apply();
    }

    public static boolean getLoginPreferences( Context context ) {
        SharedPreferences sharedPref = context.getSharedPreferences(ConstantUtil.PREFERENCES_NAME_LOGIN, Context.MODE_PRIVATE );

        return sharedPref.getBoolean( ConstantUtil.PREFERENCES_AUTO_LOGIN, false );
    }
}
