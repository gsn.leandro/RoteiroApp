package br.com.brasilreg.roteiro.app.view.inspection;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.util.StatusUtil;

import static br.com.brasilreg.roteiro.app.R.color.background_nht_rejected;
import static br.com.brasilreg.roteiro.app.R.color.background_nht_request;

public class InspectionAdapter extends RecyclerView.Adapter<InspectionAdapter.ViewHolder> {

    private final List<VehicleInspection> inspections;
    private final Context context;

    static class ViewHolder extends RecyclerView.ViewHolder  {

        TextView NHT;
        TextView cityUF;
        TextView carPlate;
        TextView limitDate;
        TextView inspectionID;
        TextView appointment;
        TextView companyName;
        TextView inspectionType;
        TextView repairShopName;
        TextView repairShopPhone;
        TextView repairShopAddress;

        ImageView appointmentIcon;

        LinearLayout statusColor;
        ConstraintLayout inspectionCard;

        ViewHolder(final View itemView) {
            super(itemView);

            NHT                 = itemView.findViewById(R.id.tvInspectionNHTCount);
            cityUF              = itemView.findViewById(R.id.tvInspectionCityUf);
            carPlate            = itemView.findViewById(R.id.tvInspectionCarPlate);
            limitDate           = itemView.findViewById(R.id.tvInspectionLimitDate);
            statusColor         = itemView.findViewById(R.id.statusInspection);
            appointment         = itemView.findViewById(R.id.tvInspectionAppointmentValue);
            companyName         = itemView.findViewById(R.id.tvInspectionCia);
            inspectionID        = itemView.findViewById(R.id.tvInspectionID);
            inspectionCard      = itemView.findViewById(R.id.inspectionCard);
            inspectionType      = itemView.findViewById(R.id.tvInspectionType);
            repairShopName      = itemView.findViewById(R.id.tvInspectionCarWorkshop);
            appointmentIcon     = itemView.findViewById(R.id.iv_inspection_appointment);
            repairShopPhone     = itemView.findViewById(R.id.tvInspectionPhone);
            repairShopAddress   = itemView.findViewById(R.id.tvInspectionAddress);
        }
    }

    public InspectionAdapter(Context context, List<VehicleInspection> inspections) {
        this.inspections = inspections;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView( recyclerView );
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder( @NonNull ViewGroup viewGroup, int i ) {
        return new ViewHolder( LayoutInflater.from(viewGroup.getContext()).inflate( R.layout.card_view_inspection, viewGroup, false ) );
    }

    @Override
    public void onBindViewHolder( @NonNull ViewHolder personViewHolder, int i ) {
        final VehicleInspection inspection = inspections.get(i);

        personViewHolder.cityUF.setText( String.format( "%s %s/%s", context.getResources().getString(R.string.card_inspection_city_address), inspection.getCity(), inspection.getUF() ) );
        personViewHolder.carPlate.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_plate_car), inspection.getCarPlate() ) );
        personViewHolder.limitDate.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_limit_date), inspection.getLimitDate() ) );
        personViewHolder.companyName.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_cia), inspection.getCompanyName() ) );
        personViewHolder.inspectionID.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_id), inspection.getInspectionID() ) );
        personViewHolder.inspectionType.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_type), inspection.getType() ) );
        personViewHolder.repairShopName.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_car_workshop), inspection.getRepairShopName() ) );
        personViewHolder.repairShopPhone.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_phone), inspection.getPhone() ) );
        personViewHolder.repairShopAddress.setText( String.format( "%s %s", context.getResources().getString(R.string.card_inspection_address), inspection.getRepairShopAddress() ) );

        personViewHolder.statusColor.setBackground( AppCompatResources.getDrawable( context, StatusUtil.getStatusColor( inspection.getStatus() ) ) );

        if ( inspection.getAppointment() == null || inspection.getAppointment().equals("24:00") ) {
            personViewHolder.appointment.setVisibility( View.INVISIBLE );
            personViewHolder.appointmentIcon.setVisibility( View.INVISIBLE );
        } else {
            personViewHolder.appointment.setVisibility( View.VISIBLE );
            personViewHolder.appointmentIcon.setVisibility( View.VISIBLE );
            personViewHolder.appointment.setText( inspection.getAppointment() );
        }

        if ( inspection.getCountNHT() == 0 )
            personViewHolder.NHT.setVisibility( View.INVISIBLE );

        if ( inspection.getNHT() == 1 )
            personViewHolder.inspectionCard.setBackground( AppCompatResources.getDrawable( context, background_nht_request ) );
        else if ( inspection.getNHT() == 2 )
            personViewHolder.inspectionCard.setBackground( AppCompatResources.getDrawable( context, background_nht_rejected ) );

        personViewHolder.itemView.setOnClickListener(view -> context.startActivity( new Intent(context, InspectionViewActivity.class).putExtra( "id", inspection.getId() ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK) ));
    }

    @Override
    public int getItemCount() {
        return inspections.size();
    }
}
