package br.com.brasilreg.roteiro.app.dao;

import java.util.List;

import br.com.brasilreg.roteiro.app.domain.VehicleInspection;

public class VehicleInspectionDAO {

    public static VehicleInspection getByID( long id ) {
        return VehicleInspection.findById( VehicleInspection.class, id );
    }

    public static void deleteAll() {
        VehicleInspection.deleteAll( VehicleInspection.class );
    }

    public static List<VehicleInspection> listAll() {
        return VehicleInspection.listAll( VehicleInspection.class );
    }

    public static void save( VehicleInspection inspection ) {
        inspection.save();
    }
}
