package br.com.brasilreg.roteiro.app.view.login.ui.login;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.Preferences;
import br.com.brasilreg.roteiro.app.dao.SurveyorDAO;
import br.com.brasilreg.roteiro.app.dao.VehicleInspectionDAO;
import br.com.brasilreg.roteiro.app.service.RequestBody;
import br.com.brasilreg.roteiro.app.service.RetrofitConfig;
import br.com.brasilreg.roteiro.app.service.response.LoginResponse;
import br.com.brasilreg.roteiro.app.view.drawer.DrawerActivity;
import br.com.brasilreg.roteiro.app.view.password.ForgotPasswordActivity;
import br.com.brasilreg.roteiro.app.view.password.UpdatePasswordActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    private CheckBox checkBox;
    private Button loginButton;
    private Button forgetPasswordButton;
    private EditText usernameEditText;
    private EditText passwordEditText;
    private ImageView showPassword;
    private ProgressBar loadingProgressBar;

    private static final int PERMISSION_REQUEST_CODE = 101;
    @SuppressLint("InlinedApi")
    private final String[] permissions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.INTERNET };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        getPermissions();

        String message = getIntent().getStringExtra("message");
        if ( message != null )
            Snackbar.make( findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT ).show();
        else
            loadPreferences();

        initUI();
        textChangedLogic();
        viewModel();
        onClickListener();
    }

    private void viewModel() {
        loginViewModel = new ViewModelProvider( this, new LoginViewModelFactory() ).get( LoginViewModel.class );

        loginViewModel.getLoginFormState().observe(this, loginFormState -> {
            if ( loginFormState == null )
                return;

            loginButton.setEnabled( loginFormState.isDataValid() );
            if ( loginFormState.getUsernameError() != null )
                usernameEditText.setError(getString( loginFormState.getUsernameError() ) );
            if ( loginFormState.getPasswordError() != null )
                passwordEditText.setError(getString( loginFormState.getPasswordError() ) );
        } );

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            if ( loginResult == null )
                return;

            if ( loginResult.getError() != null )
                showLoginFailed( loginResult.getError() );
            if ( loginResult.getSuccess() != null )
                updateUiWithUser( getCurrentFocus(), usernameEditText.getText().toString(), passwordEditText.getText().toString() );

            setResult(Activity.RESULT_OK);
        } );

        passwordEditText.setOnEditorActionListener( ( v, actionId, event ) -> {
            if ( actionId == EditorInfo.IME_ACTION_DONE )
                loginViewModel.login( usernameEditText.getText().toString(), passwordEditText.getText().toString() );

            return false;
        } );
    }

    private void onClickListener() {

        loginButton.setOnClickListener( v -> {
            loginButton.setEnabled( false );
            loadingProgressBar.setVisibility( View.VISIBLE );
            loginViewModel.login( usernameEditText.getText().toString(), passwordEditText.getText().toString() );
        });

        forgetPasswordButton.setOnClickListener(v -> startActivity( new Intent(LoginActivity.this, ForgotPasswordActivity.class) ) );

        showPassword.setOnClickListener(v -> {
            if ( passwordEditText.getTransformationMethod() != null )
                passwordEditText.setTransformationMethod( null );
            else
                passwordEditText.setTransformationMethod( new PasswordTransformationMethod() );

            passwordEditText.setSelection( passwordEditText.getText().length() );
        });
    }

    private void textChangedLogic() {

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { } // ignore

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { } // ignore

            @Override
            public void afterTextChanged(Editable editable) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(), passwordEditText.getText().toString());
            }
        };

        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
    }

    private void initUI() {
        checkBox                = findViewById( R.id.checkBox );
        loginButton             = findViewById(R.id.login);
        showPassword            = findViewById( R.id.iv_login_show_password );
        usernameEditText        = findViewById(R.id.username);
        passwordEditText        = findViewById(R.id.password);
        loadingProgressBar      = findViewById(R.id.loading);
        forgetPasswordButton    = findViewById(R.id.forget_password);
    }

    private void getPermissions() {
        ActivityCompat.requestPermissions(LoginActivity.this, permissions, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );

        if ( requestCode == 101 ) {
            for ( int grantResult : grantResults ) {
                if ( grantResult != PackageManager.PERMISSION_GRANTED ) {
                    new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.login_permissions_title))
                            .setMessage(getString(R.string.login_permissions_message))
                            .setPositiveButton(getString(R.string.login_permissions_allow), (dialog, which) -> getPermissions())
                            .setNegativeButton(getString(R.string.login_permissions_deny), (dialog, which) -> {
                                dialog.dismiss();
                                finish();
                            })
                            .create()
                            .show();
                    return;
                }
            }
        }
    }

    private void updateUiWithUser(View view, String user, String password ) {
        callLoginService( view, user, password );
    }

    private void callLoginService( View view, String user, String password ) {

        Call<LoginResponse> call = new RetrofitConfig().getConfig().login( RequestBody.doLogin( user, password ) );
        call.enqueue( new LoginCallBack( LoginActivity.this, view, user, password ) );
    }

    private void loadPreferences() {
        if ( Preferences.getLoginPreferences( getApplicationContext() ) )
            startActivity( new Intent( LoginActivity.this, DrawerActivity.class ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
    }

    private void showLoginFailed( @StringRes Integer errorString ) {
        Snackbar.make( getCurrentFocus(), errorString, Snackbar.LENGTH_SHORT ).show();
    }

    private static class LoginCallBack implements Callback<LoginResponse> {

        private final WeakReference<LoginActivity> reference;
        private final String user;
        private final String password;
        private final View view;

        LoginCallBack( LoginActivity context, View v, String sUser, String sPassword ) {
            reference = new WeakReference<>(context);
            view = v;
            user = sUser;
            password = sPassword;
        }

        @Override
        public void onResponse(@NotNull Call<LoginResponse> call, @NotNull Response<LoginResponse> response ) {
            /* Codigos: 1 Sucesso, -1 Credenciais inválidas, 2 Cadastro de Senha */
            reference.get().loadingProgressBar.setVisibility(View.GONE);
            reference.get().loginButton.setEnabled( true );
            if ( response.body() == null )
                Snackbar.make( view, reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
            else if ( response.body().getCode() == 2 ) {
                reference.get().startActivity( new Intent(reference.get(), UpdatePasswordActivity.class)
                        .putExtra( "user", user )
                        .putExtra( "password", password ) );
            } else if ( response.body().getCode() == 1 ) {
                SurveyorDAO.deleteAll();
                VehicleInspectionDAO.deleteAll();
                SurveyorDAO.save( response.body().getSurveyor() );

                Preferences.savePreferences( reference.get().checkBox.isChecked(), reference.get().getApplicationContext() );
                reference.get().startActivity( new Intent( reference.get(), DrawerActivity.class ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) );
            } else
                Snackbar.make( view, reference.get().getResources().getString( R.string.msg_invalid_credentials ), Snackbar.LENGTH_SHORT).show();
        }

        @Override
        public void onFailure(@NotNull Call<LoginResponse> call, @NotNull Throwable t) {
            reference.get().loginButton.setEnabled( true );
            reference.get().loadingProgressBar.setVisibility(View.GONE);
            Snackbar.make( view, reference.get().getResources().getString( R.string.msg_error_server ), Snackbar.LENGTH_SHORT).show();
        }
    }

}