package br.com.brasilreg.roteiro.app.util;

public class NumberUtil {

    public static String formatCallNumber( String phone ) {

        //phone = phone.replaceAll("( )-.", ""); -> Não passa nos testes

        phone = phone.replace("-", "");
        phone = phone.replace("(", "");
        phone = phone.replace(")", "");
        phone = phone.replace(" ", "");
        phone = phone.replace(".", "");

        return phone.trim();
    }

}
