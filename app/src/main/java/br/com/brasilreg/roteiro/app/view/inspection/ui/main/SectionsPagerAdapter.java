package br.com.brasilreg.roteiro.app.view.inspection.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.jetbrains.annotations.NotNull;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[] { R.string.tab_text_1, R.string.tab_text_2 };
    private final Context mContext;
    private final VehicleInspection inspection;

    public SectionsPagerAdapter(Context context, FragmentManager fm, VehicleInspection vehicleInspection) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mContext = context;
        inspection = vehicleInspection;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance( position + 1, inspection );
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return 2;
    }
}