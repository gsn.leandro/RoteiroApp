package br.com.brasilreg.roteiro.app.view.inspection;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.dao.VehicleInspectionDAO;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;
import br.com.brasilreg.roteiro.app.view.inspection.ui.main.SectionsPagerAdapter;

public class InspectionViewActivity extends AppCompatActivity {

    TextView titleID;
    ImageView backArrow;
    VehicleInspection inspection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspection_view);
        inspection = VehicleInspectionDAO.getByID( getIntent().getLongExtra("id", 1L) );

        initUI();
        inflateUI();
        clickListener();
    }

    private void clickListener() {
        backArrow.setOnClickListener(v -> onBackPressed());
    }

    private void inflateUI() {
        titleID.setText( String.format( "%s %s", getResources().getString(R.string.card_inspection_id), inspection.getInspectionID() ) );
    }

    private void initUI() {
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager(), inspection );
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
        tabs.setTabTextColors( ContextCompat.getColor( getApplicationContext(), R.color.white ), ContextCompat.getColor( getApplicationContext(), R.color.accent ) );

        titleID     = findViewById( R.id.title );
        backArrow   = findViewById( R.id.iv_inspection_view_back );
    }
}