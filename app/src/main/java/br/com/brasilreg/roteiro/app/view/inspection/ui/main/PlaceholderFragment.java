package br.com.brasilreg.roteiro.app.view.inspection.ui.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import br.com.brasilreg.roteiro.app.R;
import br.com.brasilreg.roteiro.app.util.NumberUtil;
import br.com.brasilreg.roteiro.app.view.nht.NHTRequestActivity;
import br.com.brasilreg.roteiro.app.domain.VehicleInspection;

import static br.com.brasilreg.roteiro.app.R.color.background_nht_rejected;
import static br.com.brasilreg.roteiro.app.R.color.background_nht_request;

public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    int indexView;
    private static VehicleInspection inspection;

    public static PlaceholderFragment newInstance( int index, VehicleInspection vehicleInspection ) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        inspection = vehicleInspection;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        indexView = 1;
        if ( getArguments() != null )
            indexView = getArguments().getInt(ARG_SECTION_NUMBER);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root;

        if ( indexView == 1 ) {
            root = inflater.inflate(R.layout.fragment_inspection_view, container, false);
            inflateInspectionView( root );
        } else {
            root = inflater.inflate(R.layout.fragment_inspection_view_observation, container, false);
            inflateInspectionObservation( root );
        }

        return root;
    }

    private void inflateInspectionObservation( View root ) {
        TextView observation = root.findViewById( R.id.tv_inspection_view_observation );
        observation.setText( inspection.getObservation() );
    }

    private void inflateInspectionView( View root ) {
        Button doCall           = root.findViewById( R.id.bt_do_call );
        Button requestNHT       = root.findViewById( R.id.bt_request_nht );

        TextView tvCia          = root.findViewById( R.id.tv_inspection_view_cia );
        TextView tvType         = root.findViewById( R.id.tv_inspection_view_type );
        TextView tvPlate        = root.findViewById( R.id.tv_inspection_view_plate );
        TextView tvCityUF       = root.findViewById( R.id.tv_inspection_view_city_uf );
        TextView tvSystem       = root.findViewById( R.id.tv_inspection_view_system );
        TextView tvVersion      = root.findViewById( R.id.tv_inspection_view_version );
        TextView tvLimitDate    = root.findViewById( R.id.tv_inspection_view_limit_date );
        TextView tvAppointment  = root.findViewById( R.id.tv_inspection_view_appointment );

        TextView tvPhone        = root.findViewById( R.id.tv_inspection_view_phone );
        TextView tvAddress      = root.findViewById( R.id.tv_inspection_view_address );
        TextView tvCarWorkshop  = root.findViewById( R.id.tv_inspection_view_car_workshop_name );

        tvCia.setText( String.format( "%s %s", getResources().getString(R.string.card_inspection_cia), inspection.getType() ) );
        tvType.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_type), inspection.getType() ) );
        tvPlate.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_plate), inspection.getCarPlate() ) );
        tvCityUF.setText( String.format( "%s %s/%s", getResources().getString(R.string.inspection_view_city_address), inspection.getCity(), inspection.getUF()  ) );
        tvSystem.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_system), inspection.getSystem() ) );
        tvVersion.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_version), inspection.getVersion() ) );
        tvLimitDate.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_limit_date), inspection.getLimitDate() ) );

        if ( inspection.getAppointment() == null || inspection.getAppointment().equals("24:00")  )
            tvAppointment.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_appointment), "NÃO" ) );
        else
            tvAppointment.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_appointment), inspection.getAppointment() ) );

        tvPhone.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_phone), inspection.getPhone() ) );
        tvAddress.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_address), inspection.getRepairShopAddress() ) );
        tvCarWorkshop.setText( String.format( "%s %s", getResources().getString(R.string.inspection_view_car_workshop), inspection.getRepairShopName() ) );

        if ( inspection.getNHT() == 1 ) {
            requestNHT.setText( getResources().getString( R.string.inspection_view_request_nht_requested) );
            requestNHT.setBackgroundTintList( ContextCompat.getColorStateList( getContext(), background_nht_request ) );
            requestNHT.setEnabled( false );
        } else if ( inspection.getNHT() == 2 ) {
            requestNHT.setText( getResources().getString( R.string.inspection_view_request_nht_rejected ) );
            requestNHT.setBackgroundTintList( ContextCompat.getColorStateList( getContext(), background_nht_rejected ) );
            requestNHT.setEnabled( false );
        } else {
            requestNHT.setEnabled( true );
            requestNHT.setOnClickListener( view -> startActivity( new Intent( getContext(), NHTRequestActivity.class ).putExtra( "id", inspection.getId() ) ));
        }

        doCall.setOnClickListener(view -> {
            if ( inspection.phoneIsValid() )
                startActivity( new Intent( Intent.ACTION_CALL, Uri.parse( "tel:" + NumberUtil.formatCallNumber( inspection.getPhone() ) ) ) );
            else
                Snackbar.make( view, getResources().getString( R.string.msg_do_call_error), Snackbar.LENGTH_SHORT ).show();
        } );
    }
}